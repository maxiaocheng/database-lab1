-- Table: public.coach

-- DROP TABLE public.coach;

CREATE TABLE public.coach
(
    id bigint NOT NULL,
    coach_id bigint NOT NULL,
    age integer,
    create_time date,
    update_time date,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT coach_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.coach
    OWNER to postgres;

-- Table: public.court

-- DROP TABLE public.court;

CREATE TABLE public.court
(
    id bigint NOT NULL,
    capacity integer,
    create_time date,
    update_time date,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    adress character varying COLLATE pg_catalog."default",
    CONSTRAINT court_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.court
    OWNER to postgres;
    
-- Table: public.court_train

-- DROP TABLE public.court_train;

CREATE TABLE public.court_train
(
    id bigint NOT NULL,
    team_id bigint,
    train_time date,
    court_id bigint,
    CONSTRAINT court_train_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.court_train
    OWNER to postgres;
-- Table: public.game

-- DROP TABLE public.game;

CREATE TABLE public.game
(
    id bigint NOT NULL,
    home_team_id bigint NOT NULL,
    visiting_team_id bigint NOT NULL,
    level integer,
    date date,
    create_time date,
    update_time date,
    refree_id bigint,
    score character varying COLLATE pg_catalog."default",
    CONSTRAINT game_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.game
    OWNER to postgres;
-- Table: public.player

-- DROP TABLE public.player;

CREATE TABLE public.player
(
    id bigint NOT NULL,
    palyer_id bigint NOT NULL,
    height double precision,
    weight double precision,
    create_time date,
    update_time date,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    team_id bigint NOT NULL,
    CONSTRAINT player_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.player
    OWNER to postgres;
-- Table: public.refree

-- DROP TABLE public.refree;

CREATE TABLE public.refree
(
    id bigint NOT NULL,
    refree_id bigint NOT NULL,
    age integer,
    create_time date,
    update_time date,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT refree_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.refree
    OWNER to postgres;
-- Table: public.team

-- DROP TABLE public.team;

CREATE TABLE public.team
(
    id bigint NOT NULL,
    create_time date,
    update_time date,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    city character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT team_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.team
    OWNER to postgres;
-- Table: public.team_coach_relation

-- DROP TABLE public.team_coach_relation;

CREATE TABLE public.team_coach_relation
(
    id bigint NOT NULL,
    team_id bigint,
    coach_id bigint,
    CONSTRAINT team_coach_relation_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.team_coach_relation
    OWNER to postgres;

 