# README #

## The purpose of the work is the acquisition of database design skills and practical skills of creating relational databases using PostgreSQL. 
## The general task consists of: 
1. Develop a model of "entity-relationship" of the subject area, chosen by the student independently, in accordance with the item "Requirements for the ER-model". 
2. Convert the developed model to scheme databases (PostgreSQL tables). 
3. Perform normalization of the database schema to the third normal form (3NF). 
4. Familiarize yourself with the PostgreSQL and pgAdmin 4 tools and enter a few rows of data into each of the tables using pgAdmin 4.